%define debug_package %{nil}

Summary: Addons for our Koji instance
Name: cern-koji-addons
Version: 1.2
Release: 1%{?dist}

Group: CERN/Utilities
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Packager: Linux.Support@cern.ch
Vendor:  CERN
License: GPL
Source: %{name}-%{version}.tar.gz

BuildArch: noarch

%description
Addons for koji build system
makeall - replace the "make sources" during SCM build
macros.koji - set _smp_ncpus_max

%prep
%setup -n %{name}-%{version}

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{_bindir} $RPM_BUILD_ROOT/etc/rpm/

install -m 755 src/makeall $RPM_BUILD_ROOT/%{_bindir}/
install -m 755 src/macros.koji $RPM_BUILD_ROOT/etc/rpm/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/makeall
/etc/rpm/macros.koji

%changelog
* Wed Sep 22 2021 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.2-1
- Added macros.koji

* Wed Jan 13 2021 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-2
- Version bump

* Fri Nov 15 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1-1
- Repackaged

* Mon May  14 2012 OULEVEY Thomas <thomas.oulevey@cern.ch> - 1.0-1
- First version
